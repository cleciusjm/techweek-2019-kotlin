data class Hello(
    val say: String = "Hello",
    val who: String
) {
    val message get() = "$say $who"
}