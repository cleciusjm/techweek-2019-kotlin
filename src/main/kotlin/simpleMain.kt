import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.system.measureTimeMillis

suspend fun main() = coroutineScope {
    println("Hello World")
    val milis = measureTimeMillis {
        val hello = Hello(who = "Teste")
        for (i in 1..10) {
            launch {
                println(hello.copy(say = if (i.isOdd()) "$i Ola Sr" else "$i Ola Sra").message)
                delay(100)
            }
        }
    }
    delay(10)
    println("finalizado em $milis ms")
}

fun Int.isOdd() = this % 2 != 0

