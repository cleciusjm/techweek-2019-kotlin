import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.html.respondHtml
import io.ktor.jackson.jackson
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.util.pipeline.PipelineContext
import kotlinx.html.*

suspend fun main() {
    val server = embeddedServer(Netty, 8080) {
        install(CallLogging)
        install(ContentNegotiation) { jackson {} }
        routing {
            get("/text") { call.respondText("Hello $name") }

            get("/json") { call.respond(Hello(who = name)) }


        }
    }
    server.start(wait = true)
}

private val PipelineContext<Unit, ApplicationCall>.name get() = this.call.parameters["name"] ?: "Ninguem"



