package crud

import com.mongodb.MongoClientURI
import org.litote.kmongo.KMongo


object Mongo {
    val db = KMongo.createClient(
        MongoClientURI("mongodb+srv://techweek:enC2YjhIHrRuczYX@cluster0-8r60n.gcp.mongodb.net/test?retryWrites=true")
    ).getDatabase("techweek-clecius")
}