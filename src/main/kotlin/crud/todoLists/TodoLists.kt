package crud.todoLists

import crud.Mongo
import org.litote.kmongo.*
import kotlin.collections.toList

interface TodoLists {

    suspend fun list(query: String?): List<TodoList>

    suspend fun count(query: String?): Long

    suspend fun insert(list: TodoList)

    suspend fun update(list: TodoList)

    suspend fun remove(id: String)

    companion object {
        fun instance(): TodoLists = TodoListsImpl()
    }

}

class TodoListsImpl : TodoLists {

    private val collection by lazy { Mongo.db.getCollection<TodoList>() }

    override suspend fun list(query: String?) =
        if (query != null) collection.find(TodoList::name regex ".*$query.*").toList()
        else collection.find().toList()

    override suspend fun count(query: String?) =
        if (query != null) collection.countDocuments(TodoList::name regex ".*$query.*")
        else collection.countDocuments()

    override suspend fun insert(list: TodoList) {
        collection.insertOne(list)
    }

    override suspend fun update(list: TodoList) {
        collection.replaceOneById(list.id, list)
    }

    override suspend fun remove(id: String) {
        collection.deleteOneById(id)
    }

}