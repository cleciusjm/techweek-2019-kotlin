package crud.todoLists

import org.bson.codecs.pojo.annotations.BsonId
import java.util.*

data class TodoList(
    @BsonId val id: String = UUID.randomUUID().toString(),
    val name: String = "",
    val items: List<TodoItem> = listOf()
) {
    val allChecked get() = items.all { it.checked }
}