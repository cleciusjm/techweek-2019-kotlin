package crud.todoLists

data class TodoItem(
    val value: String = "",
    val checked: Boolean = false
)