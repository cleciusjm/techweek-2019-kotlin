package crud

import crud.todoLists.*
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.util.pipeline.PipelineContext
import org.slf4j.event.Level

suspend fun main() {
    val server = embeddedServer(Netty, port = 8080) {
        install(ContentNegotiation) { jackson { } }
        install(CallLogging) { level = Level.INFO }
        install(CORS) {
            anyHost()
            method(HttpMethod.Options)
            method(HttpMethod.Get)
            method(HttpMethod.Post)
            method(HttpMethod.Put)
            method(HttpMethod.Delete)
        }
        routing {
            route("/rest/todos") {
                val repo = TodoLists.instance()
                get {
                    call.respond(repo.list(query))
                }
                get("size") {
                    call.respond(repo.count(query))
                }
                post {
                    repo.insert(call.receive())
                    call.respond(HttpStatusCode.OK)
                }
                put("/{id}") {
                    val list: TodoList = call.receive()
                    if (id == list.id) {
                        repo.update(list)
                        call.respond(HttpStatusCode.OK)
                    } else {
                        call.respond(HttpStatusCode.BadRequest, "Id invalido")
                    }
                }
                delete("/{id}") {
                    repo.remove(id!!)
                    call.respond(HttpStatusCode.OK)
                }
            }
        }
    }
    server.start(wait = true)
}

private val PipelineContext<Unit, ApplicationCall>.query get() = call.parameters["query"]

private val PipelineContext<Unit, ApplicationCall>.id get() = call.parameters["id"]

